# Repeated Testing

This project provides implementations to study different protocols to repeat tests and aggregate their results.

## Reference
*  Aggregating multiple test results to improve medical decision-making, PLOS Comp. Biol. 21, e1012749 (2025)

Please cite our work if you find the above implementations helpful for your own research projects.

```
@article{bottcher2025aggregating,
  title={Aggregating multiple test results to improve medical decision-making},
  author={B{\"o}ttcher, Lucas and D’Orsogna, Maria R and Chou, Tom},
  journal={PLOS Computational Biology},
  volume={21},
  number={1},
  pages={e1012749},
  year={2025}
}

```
